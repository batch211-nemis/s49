//alert("We are going to simulate an interactive webpage using DOM and fetching data from a server!");

/*
	fetch method
	Syntax:
	fetch('url', options)
	Accepts two arguments:
	url: this is the url which the request is to be made
	options parameter - used only when we need the details of the request from the user

	fetch() method in JS is used to send request in the server and load the received response in the webpage. The request and response is in JSON format
*/

//Get Post Data
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((data)=>showPosts(data));

/*
	use the fetch method to get the posts inside https://jsonplaceholder.typicode.com/posts
	make the response in json format (.then)
*/

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector("#txt-body").value,
			userId:1
		}),
		headers: { 'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())//converts the response into JSON format
	.then((data)=>{
		console.log(data);
		alert('Successfully Added!')
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
	})
})

//Mini-Activity 1
const showPosts = (posts) =>{
	let postEntries = "";

	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onClick="editPost('${post.id}')">Edit</button>
			<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

//Edit Post
const editPost = (id) =>{
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

	//Mini-Activity 2
		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;

	//Add attribute
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
} 

//Update Post
document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: { 'Content-Type': 'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())//converts the response into JSON format
	.then((data)=>{
		console.log(data);
		alert('Successfully Updated!')

		//Mini-Activity 3
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
})


//s49 ACTIVITY SOLUTION

//Delete Post
const deletePost = (id) =>{

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: 'DELETE',
		headers: { 
			'Content-Type': 'application/json; charset=UTF-8'
		},
	})
	.then((response)=>response.json())//converts the response into JSON format
	.then((data)=>{
		console.log(data);
		alert('Successfully Deleted!')

		document.querySelector(`#post-${id}`).remove();
})
}
